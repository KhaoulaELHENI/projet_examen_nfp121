
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.ArrayList;
/** Analyser des donnees d'un fichier, une donnee par ligne avec 4 informations
 * separes par des blancs : x, y, ordre (ignoree), valeur.
 */
public class LecteurFichier1 {

	private ArrayList<SimpleImmutableEntry<Position, Double>> source = new ArrayList<SimpleImmutableEntry<Position, Double>>();
    final String nomFichier = "donnees.txt";
    
	public final ArrayList<SimpleImmutableEntry<Position, Double>> getSource() {
		return source;
	}

	/** Charger l'analyseur avec les donnees du fichier "donnees.txt". */
	public void traiter() {
		try (BufferedReader in = new BufferedReader(new FileReader(this.nomFichier))) {
			String ligne = null;
			while ((ligne = in.readLine()) != null) {
				String[] mots = ligne.split("\\s+");
				assert mots.length == 4;	// 4 mots sur chaque ligne
				double valeur = Double.parseDouble(mots[3]);
				int x = Integer.parseInt(mots[0]);
				int y = Integer.parseInt(mots[1]);
				Position p = new Position(x, y);
				SimpleImmutableEntry<Position, Double> entree =  new SimpleImmutableEntry<Position, Double>(p, valeur);
				this.source.add(entree);

			}
			System.out.println("la liste des donnees a traiter a partir du fichier type 1 = " +"\n" + this.source);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) {
		LecteurFichier1 exemple1 = new LecteurFichier1();
		exemple1.traiter();
	}
}
