/**
 * FabriqueTraitement permet de construire les traitments demand�s.
 * Cette classe peut �tre compl�t�e si n�cessaire.
 * Son seul int�r�t est de pouvoir �crire les classes fournies sans
 * qu'elles provoquent des erreurs.  Les erreurs seront pour l'essentiel
 * localis�es dans FabriqueTraitementConcrete.
 *
 * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
 */

public interface FabriqueTraitement {

	SommeAbstrait somme();
	PositionsAbstrait positions();
	Donnees donnees();
	Multiplicateur multiplicateur(double facteur);
	SommeParPosition sommeParPosition();
	SupprimerPlusGrand supprimerPlusGrand(double seuil);
	SupprimerPlusPetit supprimerPlusPetit(double seuil);
	Max max();
	Min min();
	Normaliseur normaliseur(double debut, double fin);
	GenerateurXML generateurXML(String nomFichier);
	Maj maj();
}