
import java.util.*;
/** D�finir une position.  */
public class Position {
	private int x;
	private int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
		// System.out.println("...appel � Position(" + x + "," + y + ")" + " --> " + this);
	}

	public final int getX() {
		return x;
	}
	public final int getY() {
		return y;
	}

	@Override public String toString() {
		return  "(" + x + "," + y + ")";
	}


	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Position) {
			Position objectToCompare= (Position) obj;
			return (this.x == objectToCompare.x && this.y == objectToCompare.y);
		} return false;
	}

	@Override
	public int hashCode() {
		return java.util.Objects.hash(this.x , this.y) ;
	}
}
