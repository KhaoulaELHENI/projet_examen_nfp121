
/**
 * Multiplicateur transmet la valeur multipli�e par un facteur.
 *
 * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
 */
public class Multiplicateur extends Traitement {
	private double facteur;

	public Multiplicateur(double facteur) {
		this.facteur = facteur;
	}

	public void traiter(Position position, double valeur) {
		valeur = this.facteur * valeur;
		super.traiter(position, valeur);
	}
}
