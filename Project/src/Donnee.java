
public class Donnee{
	private double valeur;
	private Position position;

	
	public double getValeur() {
		return valeur;
	}

	public Position getPosition() {
		return position;
	}
	
	public Donnee(Position position, double valeur) {
		this.position= position;
		this.valeur = valeur;
	}

	@Override public String toString() {
		return "(" + "la position = "+ this.position + " et  " + "la valeur = " + this.valeur + ")" + "\n";
	}
}