
/**
 * SommeAbstrait 
 *
 * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
 */

abstract public class SommeAbstrait extends Traitement {

	public abstract double somme();

}
