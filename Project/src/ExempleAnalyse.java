
import java.io.FileNotFoundException;

/**
 * ExempleAnalyse 
 *
 * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */

public class ExempleAnalyse {

	public static void exemple1() {
		System.out.println();
		System.out.println("=== exemple1() ===");

		FabriqueTraitement traitements = new FabriqueTraitementConcrete();

		// Construire le traitement
		SommeAbstrait somme1 = traitements.somme();
		SommeAbstrait somme2= traitements.somme();
		SommeParPosition sommeParPosition = traitements.sommeParPosition();

		PositionsAbstrait positions = traitements.positions();
		Donnees donnees = traitements.donnees();
		Multiplicateur multiplicateur = traitements.multiplicateur(10.0);
		SupprimerPlusGrand supprimerPlusGrand = traitements.supprimerPlusGrand(3.0);
		SupprimerPlusPetit supprimerPlusPetit = traitements.supprimerPlusPetit(3.0);
		Max max = traitements.max();
		Min min = traitements.min();
		Normaliseur normaliseur = traitements.normaliseur(1.0,10.0);
		GenerateurXML generateurXML = traitements.generateurXML(".\\generateurXMLExemple1.xml");
		Maj maj = traitements.maj();
		
		System.out.println("Traitement : " + somme1);
	
		//Chaine de traitements
		somme1.ajouterSuivants(positions).ajouterSuivants(maj);
		maj.ajouterSuivants(min);
		min.ajouterSuivants(max);
		max.ajouterSuivants(supprimerPlusGrand);
		supprimerPlusGrand.ajouterSuivants(supprimerPlusPetit);
		supprimerPlusPetit.ajouterSuivants(donnees);
		donnees.ajouterSuivants(normaliseur);
		normaliseur.ajouterSuivants(generateurXML);
		generateurXML.ajouterSuivants(multiplicateur);
		
		// CycleException levee
		//somme1.ajouterSuivants(donnees).ajouterSuivants(donnees);
		//somme1.ajouterSuivants(somme1);
		
		// Traiter des données manuelles
		System.out.println();
		System.out.println("=== Traitements sur les donnees manuelles ===");
		System.out.println();

		somme1.gererDebutLot("manuelles");
		somme1.traiter(new Position(1, 1), 5.0);
		somme1.traiter(new Position(1, 2), 2.0);
		somme1.traiter(new Position(1, 1), -1.0);
		somme1.traiter(new Position(1, 2), 1.5);
		somme1.traiter(new Position(1, 3), 1.5);
		somme1.gererFinLot("manuelles");

		
		//Chaine de traitements
		System.out.println();
		System.out.println("Traitement : " + sommeParPosition);
		
		somme2.ajouterSuivants(normaliseur);
		somme2.ajouterSuivants(somme1);
		//Traiter des données manuelles
		System.out.println();
		System.out.println("=== Traitements sur les donnees manuelles ===");
		System.out.println();
	
		somme2.gererDebutLot("manuelles");
		somme2.traiter(new Position(1, 2), 2);
		somme2.traiter(new Position(1, 2), 5);
		somme2.traiter(new Position(1, 4), 1.5);
		somme2.gererFinLot("manuelles");

	
		System.out.println();
		// Exploiter les résultats
		System.out.println("Somme = " + somme1.somme());

		//Multiplicateur
		System.out.println("Somme apres multiplication: " + somme2.somme());

		//Position
		System.out.println("Positions.frequence(new Position(1,2)) = " + positions.frequence(new Position(1, 2)));
		
		//Normaliseur
		System.out.println(normaliseur);
	}



	public static void exemple2(String traitements) throws FileNotFoundException {
		System.out.println();
		System.out.println("=== exemple2(" + traitements + ") ===");
		System.out.println();

		// Construire les traitements
		TraitementBuilder builder = new TraitementBuilder();
		Traitement main = builder.traitement(new java.util.Scanner(traitements), null);

		System.out.println("Traitement : " + main);


		// Traiter des données manuelles
		System.out.println();
		System.out.println("=== Traitements sur les donnees manuelles ===");
		System.out.println();
		main.gererDebutLot("manuelles");
		main.traiter(new Position(1, 1), 5.0);
		main.traiter(new Position(1, 2), 2.0);
		main.traiter(new Position(1, 1), -1.0);
		main.gererFinLot("manuelles");

		// Construire l'analyseur
		Analyseur analyseur = new Analyseur(main);
		LecteurFichier1 exemple1 = new LecteurFichier1();

		System.out.println();
		System.out.println("=== Traitements sur les donnees recuperees ===");
		System.out.println();

		exemple1.traiter();
		System.out.println();
		System.out.println("=== resultats des traitements sur les donnees ===");
		System.out.println();
		// Traiter les autres sources de données : "donnees.txt", etc.
		analyseur.traiter(exemple1.getSource(),"traitement a partir d'un fichier de type TXT 1");
	}

	// Exemple pour tester Analyseur et les lecteurs de differents fichiers
	public static void exemple3() throws FileNotFoundException {
		System.out.println();
		System.out.println("=== exemple3() ===");
		System.out.println("=== Traitements sur les donnees recuperees ===");
		System.out.println();

		FabriqueTraitement traitements = new FabriqueTraitementConcrete();

		// Construire le traitement
		SommeAbstrait somme1 = traitements.somme();
		SommeAbstrait somme2 = traitements.somme();
		SommeAbstrait somme3 = traitements.somme();

		Analyseur analyseur1 = new Analyseur(somme1);
		Analyseur analyseur2 = new Analyseur(somme2);
		Analyseur analyseur3 = new Analyseur(somme3);

		//Initialiser les Lecteurs de fichiers
		LecteurFichier1 exemple1 = new LecteurFichier1();
		LecteurFichier2 exemple2 = new LecteurFichier2();
		LecteurFichier3 exemple3 = new LecteurFichier3();

		// Traiter des données 
		exemple1.traiter();
		exemple2.traiter();
		exemple3.traiter();

		System.out.println();
		System.out.println("=== resultats des traitements sur les donnees ===");
		System.out.println();

		analyseur1.traiter(exemple1.getSource(), "traitement a partir d'un fichier de type TXT 1");
		analyseur2.traiter(exemple2.getSource(), "traitemen a partir d'un fichier de type TXT 2");
		analyseur3.traiter(exemple3.getSource(), "traitement a partir d'un fichier de type XML DTD 7");
	}

	public static void main(String[] args) throws java.io.FileNotFoundException, ClassNotFoundException, CycleException {
		exemple1();
		exemple2("Somme 0 1 Positions 0 0");

		String calculs = "Positions 0 1 Max 0 1 Somme 0 1 SommeParPosition 0";
		String generateur = "GenerateurXML 1 java.lang.String NOM--genere.xml";
		String traitement1 = generateur.replaceAll("NOM", "brut") + " 3"
				+ " " + calculs + " 0"
				+ " " + "SupprimerPlusPetit 1 double 0.0 1 SupprimerPlusGrand 1 double 10.0 2"
				+ " " + generateur.replaceAll("NOM", "valides") + " 0"
				+ " " + calculs + " 0"
				+ " " + "Normaliseur 2 double 0.0 double 100.0 2"
				+ " " + generateur.replaceAll("NOM", "normalisees") + " 0"
				+ " " + calculs + " 0";

		exemple2(calculs + " 0");
		exemple2(traitement1);
		exemple3();

	}


}
