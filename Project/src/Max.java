
import java.util.ArrayList;
import java.util.Collections;
import java.util.NoSuchElementException;

/**
 * Max calcule le max des valeurs vues, quelque soit le lot.
 *
 * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
 */

public class Max extends Traitement {

	private double max; 
	private ArrayList<Double> list = new ArrayList<Double>();

	public double getMax() {
		return max;
	}

	public void traiter(Position position, double valeur) {
		list.add(valeur);

		try {
			this.max = Collections.max(list);
		}
		catch (ClassCastException | NoSuchElementException e) {
			System.out.println("Exception caught : " + e);
		}
		super.traiter(position, valeur);
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": max= " + this.max);
	}
}
