
import java.io.*;

/** Analyser des donn�es d'un fichier, une donn�e par ligne avec 4 informations
 * s�par�es par des blancs : x, y, ordre (ignor�e), valeur.
 */
public class AnalyseurInitial {
	/** Charger l'analyseur avec les donn�es du fichier "donnees.java". */
	public void traiter() {
		try (BufferedReader in = new BufferedReader(new FileReader("./donnees.txt"))) {
			double somme = 0.0;
			String ligne = null;
			while ((ligne = in.readLine()) != null) {
				String[] mots = ligne.split("\\s+");
				assert mots.length == 4;	// 4 mots sur chaque ligne
				double valeur = Double.parseDouble(mots[3]);
				int x = Integer.parseInt(mots[0]);
				int y = Integer.parseInt(mots[1]);
				Position p = new Position(x, y);
				somme += valeur;
				System.out.println(valeur);
				System.out.println(p);
			}
			System.out.println(somme);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) {
		new AnalyseurInitial().traiter();
	}
}
