
import java.util.ArrayList;
import java.util.Collections;
import java.util.NoSuchElementException;

/**
 * Min calcule le min des valeurs vues.
 *
 * 
 */
public class Min extends Traitement {
	private double min; 
	private ArrayList<Double> list = new ArrayList<Double>();

	public double getMin() {
		return min;
	}

	public void traiter(Position position, double valeur) {
		list.add(valeur);

		try {
			this.min = Collections.min(list);
		}
		catch (ClassCastException | NoSuchElementException e) {
			System.out.println("Exception caught : " + e);
		}
		super.traiter(position, valeur);
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": min= " + this.min);
	}
}
