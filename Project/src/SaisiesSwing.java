
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class SaisiesSwing extends JFrame {

	private String nomFichier;
	private ArrayList<String> donneesSaisies= new ArrayList<String>();

	private int numeroDordre = 0;

	//Instanciation de la fenetre
	JFrame fenetre = new JFrame("Saisie donnees");

	//Instanciation des JLabel JTextField JButton
	final private JLabel abscisse = new JLabel("Abscisse");
	final private JLabel ordonnee = new JLabel("Ordonnee");
	final private JLabel valeur = new JLabel("Valeur");

	final private JTextField abscisseText = new JTextField(15);
	final private JTextField ordonneeText = new JTextField(15);
	final private JTextField valeurText = new JTextField(15);

	final private JButton valider= new JButton("Valider");
	final private JButton effacer = new JButton("Effacer");
	final private JButton terminer = new JButton("Terminer");


	public SaisiesSwing(String nomFichier) {
		super();
		this.nomFichier = nomFichier;

		//Instanciation des composants JPanel
		JPanel component1 = new JPanel();
		JPanel component2 =new JPanel();
		JPanel component3 =new JPanel();

		//Ajout des Jlabel dans le composant 1 
		abscisse.setHorizontalAlignment(SwingConstants.CENTER);
		valeur.setHorizontalAlignment(SwingConstants.CENTER);
		ordonnee.setHorizontalAlignment(SwingConstants.CENTER);

		component1.add(abscisse);
		component1.add(ordonnee);
		component1.add(valeur);

		//affichage sur une ligne gestionnaire de placement GridLayout
		component1.setLayout(new GridLayout(1,1));

		//Ajout des JTextField dans le composant 2 
		component2.add(abscisseText);
		component2.add(ordonneeText);
		component2.add(valeurText);

		//affichage sur une ligne
		component2.setLayout(new GridLayout(1,1));

		//Ajout des JButton dans le composant3
		component3.add(valider);
		component3.add(effacer);
		component3.add(terminer);

		//Ajout des Actions dans les boutons
		this.terminer.addActionListener(new ActionTerminer());
		this.effacer.addActionListener(new ActionEffacer());
		this.valider.addActionListener(new ActionValider());

		//Ajout des composants dans la fenetre
		this.fenetre.add(component1);
		this.fenetre.add(component2);
		this.fenetre.add(component3);
		this.fenetre.setLayout(new GridLayout(3,1));

		fenetre.setVisible(true);
		fenetre.setLocation(50, 50);
		fenetre.pack();

		//Le bouton Fermer de la fenetre
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		SaisiesSwing exemple = new SaisiesSwing("SaisieSwingExemple1.txt");
	}

	//***************************************************************************************************
	// Les  classes Actions ActionListener d�finit une seule m�thode actionPerformed
	// interets une classe interne peut avoir acc�s aux m�thodes est attributs de la classe englobante
	//les boutons et textfield
	public class ActionEffacer implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == effacer){
				reset();
			}
		}
	}

	public class ActionTerminer implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			Terminer();
			System.out.println("votre fichier '" + nomFichier + "' est enregistre");
			System.exit(1);
		}

	}

	public class ActionValider implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == valider){
				verifierSaisie(abscisseText);
				verifierEntier(abscisseText);

				verifierSaisie(ordonneeText);
				verifierEntier(ordonneeText);

				verifierSaisie(valeurText);
				verifierDouble(valeurText);

				if(verifierEntier(abscisseText) == true && 	verifierEntier(ordonneeText)== true &&verifierDouble(valeurText)== true) {
					String espace = " ";
					donneesSaisies.add(abscisseText.getText());
					donneesSaisies.add(espace);
					donneesSaisies.add(ordonneeText.getText());
					donneesSaisies.add(espace);
					donneesSaisies.add(Integer.toString(numeroDordre++));
					donneesSaisies.add(espace);
					donneesSaisies.add(valeurText.getText());
					reset();
				}
			}
		}
	}

	//***************************************************************************************************
	// Les  Methodes de verification des saisie

	/**
	 * verifie champ vide 
	 * @param field
	 */
	public void verifierSaisie(JTextField field) {
		if (field.getText().isEmpty()) {
			field.setBackground(Color.RED);
		}
		else
			field.setBackground(Color.WHITE);
	}

	/**
	 * verifie si c'est un entier
	 * @param field
	 * @return
	 */
	private boolean  verifierEntier(JTextField field) {	
		try
		{
			Integer.parseInt(field.getText());
			return true;
		}catch(NumberFormatException  e)
		{
			field.setBackground(Color.RED);
			return false;
		}
	}

	/**
	 * verifie si c'est un double
	 * @param field
	 * @return
	 */
	private boolean  verifierDouble(JTextField field) {
		try
		{
			Double.parseDouble(field.getText());
			return true;
		}catch(NumberFormatException | NullPointerException e)
		{
			field.setBackground(Color.RED);
			return false;
		}
	}

	/**
	 * remet tout les chmaps � z�ro
	 */
	private void reset() {
		ArrayList<JTextField> fieldList = new ArrayList<JTextField>();
		fieldList.add(abscisseText);
		fieldList.add(ordonneeText);
		fieldList.add(valeurText);
		for(JTextField field : fieldList) {
			field.setText("");
			field.setBackground(Color.WHITE);
		}
	}

	/**
	 * affiche les parametres saisies et cree le fichier associe
	 */
	private void Terminer() {
		File file = new File(nomFichier);
		try {
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter(file));
			String ligne = "";
			for(int i=0, j=1; i<donneesSaisies.size(); j++, i ++ ) {
				ligne += donneesSaisies.get(i);

				if(j%7 == 0) {
					System.out.println(ligne);
					fileWriter.newLine();
					fileWriter.write(ligne);
					ligne="";
				}
			}
			fileWriter.flush();
			fileWriter.close();

		}catch(IOException e){
			e.printStackTrace();
		}
	}
}

