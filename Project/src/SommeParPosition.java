
import java.util.*;

/**
 * SommeParPosition 
 *
 * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
 */

public class SommeParPosition extends Traitement {

	private HashMap<Position, Double> donnees = new HashMap<Position, Double>();

	public void traiter(Position position, double valeur) {

		if(this.donnees.get(position)==null) {
			this.donnees.put(position, valeur);
		} else {
			double sommeParPosition = this.donnees.get(position);
			sommeParPosition +=valeur;

			this.donnees.put(position, sommeParPosition);
		}
		super.traiter(position, valeur);
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": somme par position = " + this.donnees);
	}
}


