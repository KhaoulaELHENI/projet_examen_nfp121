
import java.util.ArrayList;

/**
 * Donnees enregistre toutes les donn�es re�ues, quelque soit le lot.
 *
 * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
 */
public class Donnees extends Traitement {

	private ArrayList<Donnee> donnees= new ArrayList<Donnee>();
	
	@Override
	public void traiter(Position position, double valeur) {
		Donnee nouvelleDonnee = new Donnee(position, valeur);
		this.donnees.add(nouvelleDonnee);
		super.traiter(position, valeur);
	}	

	public ArrayList<Donnee> getDonnees() {
		return donnees;
	}
/*
	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": Donnees = " );
		String results = "";
	    for (int i = 0; i <this.donnees.size(); ++i)
	    {
	    	 results += "Donnee " + i + " = "+ this.donnees.get(i);
	    }
		System.out.println(results);
	}
	*/
}