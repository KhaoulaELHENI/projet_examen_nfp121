
import java.lang.reflect.*;
import java.util.*;

/**
 * TraitementBuilder 
 *
 * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
 */
public class TraitementBuilder {


	/** Retourne un objet de type Class correspondant au nom en param�tre.
	 * Exemples :
	 *   - int donne int.class
	 *   - Normaliseur donne Normaliseur.class
	 */
	public Class<?> analyserType(String nomType)throws ClassNotFoundException {
		Class<?> c;
		switch(nomType) {
		case "int":
			c = int.class;
			break;
		case "double":
			c = double.class;
			break;
		case "String":
			c = String.class;
			break;
		default:
			c = Class.forName(nomType);
		}
		return c;
	}

	/** Cr�e l'objet java qui correspond au type formel en exploitant le � mot � suviant du scanner.
	 * Exemple : si formel est int.class, le mot suivant doit �tre un entier et le r�sulat est l'entier correspondant.
	 * Ici, on peut se limiter aux types utlis�s dans le projet : int, double et String.
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	static Object decoderEffectif(Class<?> formel, Scanner in) throws InstantiationException, IllegalAccessException{

		Object o = null;

		if (formel.equals(int.class))
		{
			int i = Integer.parseInt(in.next());
			o = i;
		}
		else
			if (formel.equals(double.class))
			{
				double d = Double.parseDouble(in.next());
				o = d;
			}	else
				if (formel.equals(String.class))
				{
					String str = in.next();
					o = str;
				}	
				else {
					o = formel.newInstance();
				}
		return o;
	}



	/** Analyser une signature pour retrouver les param�tres formels et les param�tres effectifs.
	 * Exemple � 3 double 0.0 java.lang.String xyz int -5 � donne
	 *   - [double.class, String.class, int.class] pour les param�tres effectifs et
	 *   - [0.0, "xyz", -5] pour les param�tres formels.
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	Signature analyserSignature(Scanner in) throws ClassNotFoundException,  InstantiationException, IllegalAccessException {
		// lecture de nombre de parametres 
		int numParam = Integer.parseInt(in.next());
		Object[] effectifs =new Object[numParam];
		Class<?>[] formels = new Class[numParam];
		Class<?> formel = null;
		Object effectif= null;

		for (int i= 0; i<numParam; i++)
		{	
			//recuperation des param�tres formels
			formel = analyserType(in.next());
			formels[i]= formel;

			//recup�ration des param�tres effectifs
			effectif = decoderEffectif(formel, in);
			effectifs[i] = effectif;
		}
		//recuperation de la signature d'un traitement
		Signature signature = new Signature(formels, effectifs);
		//System.out.println(signature.toString());
		return signature;
	}


	/** Analyser la cr�ation d'un objet.
	 * Exemple : � Normaliseur 2 double 0.0 double 100.0 � consiste � charger
	 * la classe Normaliseur, trouver le constructeur qui prend 2 double, et
	 * l'appeler en lui fournissant 0.0 et 100.0 comme param�tres effectifs.
	 */
	Object analyserCreation(Scanner in)
			throws ClassNotFoundException, InvocationTargetException,
			IllegalAccessException, NoSuchMethodException,
			InstantiationException
	{
		String type  = in.next();
		Signature signature =  this.analyserSignature(in); 
		Class<?> classe = 	this.analyserType(type);

		//recuperation du constructeur � travers les parametres formels
		Constructor<?> constructeur = classe.getConstructor(signature.getFormels());

		//creation d'un objet traitement avec les parametres effectifs � travers son constructeur
		Object objet = constructeur.newInstance(signature.getEffectifs());
		return objet;
	}




	/** Analyser un traitement.
	 * Exemples :
	 *   - � Somme 0 0 �
	 *   - � SupprimerPlusGrand 1 double 99.99 0 �
	 *   - � Somme 0 1 Max 0 0 �
	 *   - � Somme 0 2 Max 0 0 SupprimerPlusGrand 1 double 20.0 0 �
	 *   - � Somme 0 2 Max 0 0 SupprimerPlusGrand 1 double 20.0 1 Positions 0 0 �
	 * @param in le scanner � utiliser
	 * @param env l'environnement o� enregistrer les nouveaux traitements
	 */
	Traitement analyserTraitement(Scanner in, Map<String, Traitement> env)
			throws ClassNotFoundException, InvocationTargetException,
			IllegalAccessException, NoSuchMethodException,
			InstantiationException
	{
		Traitement traitement = (Traitement) this.analyserCreation(in);

		int numTraitementSuivants = Integer.parseInt(in.next());

		for (int i= 0; i<numTraitementSuivants; i++)
		{	
			traitement.ajouterSuivants(analyserTraitement(in, env));
		}
		return traitement;   
	}


	/** Analyser un traitement.
	 * @param in le scanner � utiliser
	 * @param env l'environnement o� enregistrer les nouveaux traitements
	 */
	public Traitement traitement(Scanner in, Map<String, Traitement> env)
	{
		try {
			return analyserTraitement(in, env);
		} catch (Exception e) {
			throw new RuntimeException("Erreur sur l'analyse du traitement, "
					+ "voir la cause ci-dessous", e);
		}
	}
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException  {
		TraitementBuilder exemple = new TraitementBuilder();
		exemple.analyserType("Position");
		Scanner in0= new Scanner("13 12.5 texte fin");
		exemple.decoderEffectif(int.class, in0);
		Scanner in1 = new Scanner("2 int 1 double 2");
		exemple.analyserSignature(in1);
		Scanner in2 =new Scanner("Position 2 int 5 int 3");
		exemple.analyserCreation(in2);
		Scanner in3 =new Scanner("Somme 0 2 Max 0 0 SupprimerPlusGrand 1 double 20.0 1 Positions 0 0");
		exemple.analyserTraitement(in3, null) ;
	}


	/** D�finition de la signature, les param�tres formels, mais aussi les param�tres effectifs.  */
	public static class Signature {

		Class<?>[] formels;
		Object[] effectifs;
		public final Class<?>[] getFormels() {
			return formels;
		}

		public final Object[] getEffectifs() {
			return effectifs;
		}
		public Signature(Class<?>[] formels, Object[] effectifs) {
			this.formels = formels;
			this.effectifs = effectifs;
		}

		/*
		@Override public String toString() {
			String results = "Param�tres Effectifs et Formels"+ "\n";

			for (int i = 0; i <this.formels.length; ++i)
			{
				results +=  this.formels[i]+ "= ";
				results +=  this.effectifs [i]+ "\n";
			}

			return results;
		}
		 */
	}

}