
import java.util.*;

/**
  * PositionsAbstrait sp�cifie un traitement qui m�morise
  * toutes les positions trait�es pour ensuite y acc�der
  * par leur indice ou obtenir la fr�quence d'une position.
  *
  * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
  */

abstract public class PositionsAbstrait extends Traitement {

	/** Obtenir le nombre de positions m�moris�es.
	 * @return le nombre de positions m�moris�es
	 */
	public abstract int nombre();

	/** Obtenir la i�me position enregistr�e.
	 * @param numero num�ro de la position souhait�e (0 pour la premi�re)
	 * @return la position de num�ro d'ordre `numero`
	 * @exception IndexOutOfBoundsException si le numero est incorrect
	 */
	public abstract Position position(int indice);

	/** Obtenir la fr�quence d'une position dans les positions trait�es.
	 * @param position la position dont on veut conna�tre la fr�quence
	 * @return la fr�quence de la position en param�tre
	 */
	public abstract int frequence(Position position);

}
