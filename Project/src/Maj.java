import java.util.ArrayList;
import java.util.HashMap;

public class Maj extends Traitement {

	private Donnees donnees;
	private Positions positions;
	private HashMap<Position, Double> positionsMaj = new HashMap<Position, Double>();

	public void traiter(Position position, double valeur) {
		this.donnees.traiter(position, valeur);
		this.positions.traiter(position, valeur);
		super.traiter(position, valeur);
	}
	/***
	 * verifie si une position est redondante et enregistre la derniere valeur
	 * @param nomLot
	 * @returnHashMap<Position, Double> positions
	 */
	private HashMap<Position, Double> positions(String nomLot){

		ArrayList<Donnee> donneesList = this.donnees.getDonnees();
		ArrayList<Position> positionsList = this.positions.getPositions();

		for(int i=0;  i<positionsList.size(); i++) {
			Position position = donneesList.get(i).getPosition();
			double valeur = donneesList.get(i).getValeur();
			Position positionRedondante = positionsList.get(i);

			if (this.positions.frequence(positionRedondante)>1 && position.equals(positionRedondante))
			{
				this.positionsMaj.put(position, valeur );
			}
		}

		System.out.println("Les positions qui sont mis a jour = " + this.positionsMaj);
		return this.positionsMaj;
	}

	@Override
	public void gererDebutLotLocal(String nomLot) {
		this.donnees = new Donnees();
		this.positions = new Positions();
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		this.positions(nomLot);
	}
}
