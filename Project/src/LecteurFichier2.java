
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.AbstractMap.SimpleImmutableEntry;

/** Analyser des donnees d'un fichier, une donnee par ligne avec 6 informations
 * separes par des blancs : identifiant (ignoree), x, y, un mot (ignoree), valeur, une lettre (ignoree)
 */
public class LecteurFichier2 {
	private ArrayList<SimpleImmutableEntry<Position, Double>> source = new ArrayList<SimpleImmutableEntry<Position, Double>>();
	final String nomFichier = "donnees-erreurs.txt";

	public final ArrayList<SimpleImmutableEntry<Position, Double>> getSource() {
		return source;
	}

	/** Charger l'analyseur avec les donnees du fichier "donnees-erreurs.txt". */
	public void traiter() {
		try (BufferedReader in = new BufferedReader(new FileReader(this.nomFichier))) {
			String ligne = null;
			while ((ligne = in.readLine()) != null) {
				String[] mots = ligne.split("\\s+");
				assert mots.length == 5;	// 5 mots sur chaque ligne
				double valeur = Double.parseDouble(mots[4]);
				int x = Integer.parseInt(mots[1]);
				int y = Integer.parseInt(mots[2]);
				Position p = new Position(x, y);
				SimpleImmutableEntry<Position, Double> entree =  new SimpleImmutableEntry<Position, Double>(p, valeur);

				this.source.add(entree);

			}
			System.out.println("la liste des donnees a traiter a partir du fichier type 2 = " +"\n"+ this.source);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) {
		LecteurFichier2 exemple2 = new LecteurFichier2();
		exemple2.traiter();
	}

}
