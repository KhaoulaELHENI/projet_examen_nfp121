
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import org.jdom2.Attribute;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;


public class GenerateurXML extends Traitement{
	private static String nomFichier;
	private ArrayList<Donnee> donnees= new ArrayList<Donnee>();
	private Donnees donneesRecupere;

	public GenerateurXML(String nomFichier) {
		super();
		GenerateurXML.nomFichier = nomFichier;
	}

	public void traiter(Position position, double valeur) {
		this.donneesRecupere.traiter(position, valeur);
		super.traiter(position, valeur);
	}


	@Override
	public void gererDebutLotLocal(String nomLot) {
		this.donneesRecupere = new Donnees();
	}

	@Override
	public void gererFinLotLocal(String nomLot) {

		ArrayList<Donnee> donneesList = this.donneesRecupere.getDonnees();

		// Creation de l'element racine avec le nom du lot
		Element racine = new Element("donnees");
		racine.setText(nomLot);

		for (int i=0; i< donneesList.size(); i++)
		{
			//recuperation des donnees
			Position position = donneesList.get(i).getPosition();
			double valeur = donneesList.get(i).getValeur();
			Donnee nouvelleDonnee = new Donnee(position, valeur);
			this.donnees.add(nouvelleDonnee);

			// Creation de l�element "donnee"
			Element donnee = new Element("donnee");

			// Creation de l�atribut "id"
			Attribute id = new Attribute("id", Integer.toString(i));
			// Ajout de l�atribut "id" dans element donnee
			donnee.setAttribute(id);

			Attribute x = new Attribute("x", Integer.toString(this.donnees.get(i).getPosition().getX()));
			donnee.setAttribute(x);

			// Creation de l'elemnt fils "y"
			Element y = new Element("y").setText(Integer.toString(this.donnees.get(i).getPosition().getY()));

			// Creation de l'elemnt fils "valeur"
			Element value = new Element("valeur").setText(Double.toString(this.donnees.get(i).getValeur()));

			// Ajout des fils dans element donnee
			donnee.addContent(y);
			donnee.addContent(value);

			// Ajout de donnee dans racine
			racine.addContent(donnee);
		}
		// Creation du document XML 
		DocType docType = new DocType( "generateur.dtd");
		Document document = new Document(racine, docType);

		// generation du document XML
		genererFichier(document, System.out);

		//Affichage du document XML
		//System.out.println(nomLot + ": Generateur XML = " );
		//afficherConsole(document, System.out);
	}

	/***
	 * Affiche les donnees sur la console
	 * @param document
	 * @param out
	 */
	public static void afficherConsole(Document document, OutputStream out) {
		try {
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, out);
		} catch (java.io.IOException e) {
			throw new RuntimeException("Erreur sur �criture : ", e);
		}
	}

	/**
	 * Genere le fichier xml
	 * @param document
	 * @param out
	 */
	public static void genererFichier(Document document, OutputStream out) {
		try {
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			sortie.output(document, new FileWriter(nomFichier));
		}
		catch (IOException io) 
		{
			io.printStackTrace();
		} 
	}
}
