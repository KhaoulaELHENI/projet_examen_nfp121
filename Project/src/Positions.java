
import java.util.*;

/**
 * Positions enregistre toutes les positions, quelque soit le lot.
 *
 * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
 */

public class Positions extends PositionsAbstrait {

	private ArrayList<Position> positions= new ArrayList<Position>();

	@Override
	public int nombre() {
		int positionsnumber = positions.size();
		return positionsnumber; 
	}

	@Override
	public Position position(int indice) {
		Position position= this.positions.get(indice);
		return position;
	}

	@Override
	public int frequence(Position position) {
		int frequence = Collections.frequency(this.positions,  position);
		return frequence;
	}

	public void traiter(Position position, double valeur) {
		this.positions.add(position);
		super.traiter(position, valeur);
	}

	public ArrayList<Position> getPositions() {
		return positions;
	}
}

