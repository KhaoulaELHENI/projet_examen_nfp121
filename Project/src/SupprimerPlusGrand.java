
import java.util.ArrayList;

/**
 * SupprimerPlusGrand supprime les valeurs plus grandes qu'un seuil.
 *
 * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
 */
public class SupprimerPlusGrand extends Traitement {

	private double seuil;
	private ArrayList<Double> valeurs= new ArrayList<Double>();

	public SupprimerPlusGrand(double seuil) {
		super();
		this.seuil = seuil;
	}

	public void traiter(Position position, double valeur) {
		this.valeurs.add(valeur);
		for (int i=0; i<this.valeurs.size(); i++)
		{
			valeur = this.valeurs.get(i);
			if (valeur > this.seuil) {
				this.valeurs.remove(i);
			}
		}
		super.traiter(position, valeur);
	}
}
