
import java.util.ArrayList;

/**
 * Normaliseur normalise les donn�es d'un lot en utilisant une transformation affine.
 *
 * @author	Xavier Cr�gut <Prenom.Nom@enseeiht.fr>
 */
public class Normaliseur extends Traitement {

	private Max max;
	private Donnees donnees;
	private double debut;
	private double fin;
	private ArrayList<Donnee> donneesApresTraitement= new ArrayList<Donnee>();
	
	public Normaliseur(double d, double f) {
		super();
		this.debut = d;
		this.fin = f;
	}

	public void traiter(Position position, double valeur) {
		this.max.traiter(position, valeur);
		this.donnees.traiter(position, valeur);
		super.traiter(position, valeur);
	}

	@Override
	public void gererDebutLotLocal(String nomLot) {
		this.max= new Max(); 
		this.donnees = new Donnees();
	}
	
	@Override
	public void gererFinLotLocal(String nomLot) {
		String resultat = "Resultats Normaliseur = " + "\n" ;
		ArrayList<Donnee> donneesList = this.donnees.getDonnees();
		double max = this.max.getMax();
		double min = -1 *  this.max.getMax();

		for(int i=0;  i<donneesList.size(); i++) {

			double a =  (max-min)/(this.fin-this.debut);
			double b = (this.debut -a )* min;

			double valeur = donneesList.get(i).getValeur();
			valeur =  (a * valeur)/b;
			
			Position position = donneesList.get(i).getPosition();
			Donnee nouvelDonnee= new Donnee(position, valeur);
			this.donneesApresTraitement.add(nouvelDonnee);
			resultat +=  this.donneesApresTraitement.get(i);
		}
		System.out.println(resultat );
	}

}

