
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.AbstractMap.SimpleImmutableEntry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LecteurFichier3 {

	private ArrayList<SimpleImmutableEntry<Position, Double>> source = new ArrayList<SimpleImmutableEntry<Position, Double>>();
	private double valeur;
	private int x;
	private int y;
	final String nomFichier = "generateurXMLExemple1.xml";

	public final ArrayList<SimpleImmutableEntry<Position, Double>> getSource() {
		return source;
	}

	/** Charger l'analyseur avec les donnees du fichier "generateurexemple1.xml". */
	public void traiter() {
		try 
		{
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder builder = factory.newDocumentBuilder();     
			final Document document= builder.parse(new File(this.nomFichier));
			this.recupererElements(document);
			System.out.println("la liste des donnees a traiter a partir du fichier XML au format DTD7 = " +"\n" + this.source);
		}
		catch (final ParserConfigurationException e) 
		{
			e.printStackTrace();
		}
		catch (final SAXException e) 
		{
			e.printStackTrace();
		}
		catch (final IOException e) 
		{
			e.printStackTrace();
		}
	}

	/** recuperer tout les elements d'un document et les transformer en une  ArrayList<SimpleImmutableEntry>
	 * @param Document document
	 * 
	 */
	public void recupererElements(Document document) {
		final Element racine = document.getDocumentElement();
		final NodeList racineNoeuds = racine.getElementsByTagName("donnee");
		final int nbRacineNoeuds = racineNoeuds.getLength();

		for (int i = 0; i<nbRacineNoeuds; i++) 
		{
			if(racineNoeuds.item(i).getNodeType() == Node.ELEMENT_NODE) 
			{
				Node donnee = racineNoeuds.item(i);
				this.x = this.recupererAttributX(donnee);
				this.y = this.recupererY(donnee);
				this.valeur = this.recupererValeur(donnee);
				Position p = new Position(this.x, this.y);
				SimpleImmutableEntry<Position, Double> entree =  new SimpleImmutableEntry<Position, Double>(p, valeur);
				this.source.add(entree);
			}				
		}
	}

	/** recuperer le child valeur d'un noeud n 
	 * @param Node n 
	 * @return double valeur
	 */
	private double recupererValeur(Node n) {
		double valeur = 0;
		if (n.hasChildNodes()) {
			NodeList racineNoeuds = n.getChildNodes();
			for (int i = 0; i<racineNoeuds.getLength(); i++) {
				if(racineNoeuds.item(i).getNodeName() == "valeur") {
					racineNoeuds.item(i).getTextContent();
					valeur = Double.parseDouble(racineNoeuds.item(i).getTextContent());
				}
			}
		}
		return valeur;
	}

	/** recuperer le child y d'un noeud n 
	 * @param Node n 
	 * @return double y
	 */
	private int recupererY(Node n) {
		int y = 0;
		if (n.hasChildNodes()) {
			NodeList racineNoeuds = n.getChildNodes();
			for (int i = 0; i<racineNoeuds.getLength(); i++) {
				if(racineNoeuds.item(i).getNodeName() == "y") {
					racineNoeuds.item(i).getTextContent();
					y = Integer.parseInt(racineNoeuds.item(i).getTextContent());
				}
			}
		}	
		return y;
	}

	/** recuperer l'attribut x d'un noeud n 
	 * @param Node n 
	 * @return int x
	 */
	private int recupererAttributX(Node n) {
		int x =0;
		NamedNodeMap attributs = n.getAttributes();
		for (int i = 0; i < attributs.getLength(); i++) {
			Attr attribut = (Attr) attributs.item(i);
			if (attribut.getName() == "x") {
				x = Integer.parseInt(attribut.getValue());
			}
		} 
		return x;
	}

	public static void main(String[] args) {
		LecteurFichier3 exemple3 = new LecteurFichier3();
		exemple3.traiter();
	}
}
